import { css } from "emotion";
import { container } from "./shiva/container";
import { styles } from "./styles";

export const title = ({ desc, title }: { desc: string, title: string }) => {
    const titleStyle = css({
        fontSize: '2rem',
        color: styles.secondaryColour,
        marginBottom: '0.5rem'
    });

    const descStyle = css({
        color: styles.tertiaryColour,
        whiteSpace: 'pre-wrap',
        fontSize: '0.9rem'
    });

    const groupStyle = css({
        marginBottom: '2.4rem'
    });

    return container({
        className: groupStyle,
        children: [
            container({
                className: titleStyle,
                textContent: title
            }),
            container({
                className: descStyle,
                textContent: desc
            })
        ]
    });
};

