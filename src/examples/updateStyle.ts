import { styleReducer } from './styleReducer';
import { container, useState } from "../shiva/container";
import { css } from 'emotion';

const app = () => {

    const [small, setSmall] = useState(true);
    const [wonderful, setWonderful] = useState('erm');

    container({
        root: true,
        children: [
            styleReducer({
                small
            }),
            container({
                className: css({
                    margin: '1rem'
                }),
                textContent: wonderful,
                attributes: {
                    ['wowowowo']: 'wowowo'
                }
            })
        ]
    });

    setSmall(false);
    // setSmall(true);
    setWonderful('wonderful');
};

app();
