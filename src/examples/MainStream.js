import flyd from 'flyd';

const stream = flyd.stream();

export const subscribe = flyd.scan((state, patch) => patch, {}, stream);

export const publish = (payload) => {
    stream(payload);
};