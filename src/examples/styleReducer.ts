import { css } from 'emotion';
import { container, useState, State } from "../shiva/container";

export const styleReducer = ({ small }: { small: State }) => {

    const reduceStyle = (small: boolean) => {
        return css({
            fontSize: small ? '1rem' : '10rem',
            margin: small ? '10rem' : '1rem'
        });
    };

    const [className] = useState(small, reduceStyle);

    const heart = container({
        textContent: '💚',
        className
    });

    return heart;
};
