export const styles = {
    primaryColour: 'cornflowerblue',
    backgroundColour: 'floralwhite',
    secondaryColour: '#6d5c64',
    tertiaryColour: '#6d5c6480',
    lineColour: '#e9d3cb'
}