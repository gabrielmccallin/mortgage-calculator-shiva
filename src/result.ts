import { css } from "emotion";
import { container } from "./shiva/container";
import { styles } from "./styles";

export const result = ({ id, value, title, desc }: { id: string, value, title: string, desc: string }) => {

  const resultStyle = css({
    fontSize: '2rem',
    marginTop: '0.5rem',
    color: styles.secondaryColour
  });

  const descStyle = css({
    fontSize: '0.8rem',
    color: styles.tertiaryColour
  });

  const titleStyle = css({
    marginBottom: '0.3rem'
  });

  const groupStyle = css({
    marginBottom: '1.2rem'
  });

  const resultField = container({
    className: resultStyle,
    id,
    textContent: value
  });

  return container({
    className: groupStyle,
    children: [
      container({
        className: titleStyle,
        textContent: title
      }),
      container({
        className: descStyle,
        textContent: desc
      }),
      resultField
    ]
  });
};

