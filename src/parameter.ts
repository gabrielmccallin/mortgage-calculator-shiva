import { useState, container } from "./shiva/container";
import { StyleDeclaration } from "./shiva/styleSchema";
import { styles } from "./styles";
import { css } from "emotion";

export const parameter = ({ id, name, onChange, value, title }: { id: string, name: string, value: number, onChange: (e) => void, title: string }): HTMLElement => {

    const inputStyle = css({
        display: 'block',
        fontSize: '1.2rem',
        border: `1px solid #e9d3cb`,
        borderRadius: "0.3rem",
        padding: '0.5rem',
        outline: 'none',
        marginTop: '0.5rem',
        width: '100%',
        backgroundColor: 'transparent',
        color: styles.secondaryColour
    });

    const labelStyle = css({
        marginBottom: '2.4rem',
        display: 'block'
    });

    const onChangeGuard = (e) => {
        const target = e.target as HTMLInputElement;
        let value = target.value as string;

        if (value.match(/\D+/)) {
            value = value.replace(/\D+/, '');
            setInputValue(value);
        }
        onChange({ name: e.target.name, value: Number(value) });
    };

    // const onInput = (e: KeyboardEvent) => {
    //     console.log('asdfasfd');
    //     if (e.key === 'Enter') {
    //         const target = e.currentTarget as HTMLInputElement;
    //         console.log(target.blur);
    //         target.blur();
    //     }
    // };

    const [inputValue, setInputValue] = useState(value, (value: number) => value.toString());

    return container({
        tagName: 'label',
        className: labelStyle,
        textContent: title,
        children: container({
            tagName: 'input',
            className: inputStyle,
            attributes: {
                id,
                name,
                value: inputValue
            },
            event: [{
                name: 'input',
                handler: (e) => onChangeGuard(e)
            }]
        })
    });
};

