const monthlyFactor = 293;
const monthlyLtvBaseline = 85;
const monthlySpread = 60;
const moveCost = 5000;

export const calculate = ({ mortgage, ltv, house, savings }: { mortgage: number, ltv: number, house: number, savings: number }): { monthly: number, left: number } => {
    const stampDuty = house * 0.03;
    const deposit = house * ((100 - ltv) / 100);

    let left = savings - stampDuty - moveCost - deposit;

    const actualMortgage = house - deposit;
    if (actualMortgage > mortgage) {
        left = left - (actualMortgage - mortgage);
    }

    const monthly = (actualMortgage / monthlyFactor) + ((ltv - monthlyLtvBaseline) * monthlySpread);

    return {
        monthly,
        left
    }
};