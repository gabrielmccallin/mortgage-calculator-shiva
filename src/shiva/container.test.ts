import { populateContainerState, State, container, useState, ContainerSchema } from './container';

describe("container", function () {
    it("populateContainerState", () => {
        const element = container();
        const props = {
            textContent: <State>{
                get initial() {
                    return 'hello';
                },
                setState: () => { }
            },
            fontSize: <State>{
                get initial() {
                    return '2rem';
                },
                setState: () => { }
            }
        };

        const populatedContainerStates = populateContainerState(props, element);

        expect(props.textContent.element).toBe(element);
        expect(props.textContent.prop).toBe('textContent');
        expect(props.fontSize.element).toBe(element);
        expect(props.fontSize.prop).toBe('fontSize');

        expect(populatedContainerStates[0].prop).toEqual('textContent');
    });

    it("create container with properties", () => {
        const textContentFixture = 'hello';

        const testContainer = container({
            textContent: textContentFixture
        });

        expect(testContainer.textContent).toEqual(textContentFixture);
    });

    it("create container with style properties", () => {
        const fontSizeFixture = '2rem';

        const testContainer = container({
            fontSize: fontSizeFixture
        });

        expect(testContainer.style.fontSize).toEqual(fontSizeFixture);
    });

    it("create container with multiple properties", () => {
        const fixture: ContainerSchema = {
            fontSize: '2rem',
            textContent: 'hello',
            attributes: {
                id: 'wow'
            }
        };

        const testContainer = container(fixture);

        expect(testContainer.style.fontSize).toEqual(fixture.fontSize);
        expect(testContainer.textContent).toEqual(fixture.textContent);
        expect(testContainer.getAttribute('id')).toEqual(fixture.attributes.id);
    });

    it("create container with custom attributes", () => {
        const fixture: ContainerSchema = {
            attributes: {
                ['data-test-id']: 'wow'
            }
        };

        const testContainer = container(fixture);

        expect(testContainer.getAttribute('data-test-id')).toEqual(fixture.attributes['data-test-id']);
    });

    it("create container with children", () => {
        const fixture: ContainerSchema = {
            children: [
                container({ textContent: '1' }),
                container({ textContent: '2' })
            ]
        };

        const testContainer = container(fixture);

        expect(testContainer.children.length).toEqual(2);
        expect(testContainer.childNodes[0].textContent).toEqual('1');
        expect(testContainer.childNodes[1].textContent).toEqual('2');
        expect(testContainer.hasChildNodes()).toBeTruthy();
    });

    it("create container with attribute", () => {
        const testContainer = container({
            attributes: {
                id: 'hello'
            }
        });

        expect(testContainer.getAttribute('id')).toEqual('hello');
    });

    it("create container with className", () => {
        const testContainer = container({
            className: 'wow'
        });

        expect(testContainer.className).toEqual('wow');
    });

    it("create container with properties", () => {
        const textContentFixture = 'hello';

        const testContainer = container({
            textContent: textContentFixture
        });

        expect(testContainer.textContent).toEqual(textContentFixture);
    });

    it("create input with properties", () => {
        const testContainer = <HTMLInputElement>container({
            tagName: 'input',
            attributes: {
                value: 'hello'
            }
        });

        expect(testContainer.value).toEqual('hello');
    });


});
