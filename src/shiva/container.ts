import { StyleDeclaration } from "./styleSchema";
export { useState } from './state';

export interface EventSchema {
	name: string;
	handler: (e) => void;
}

export interface ContainerSchema extends StyleDeclaration {
	className?: string | State;
	id?: string | State;
	tagName?: string | State;
	children?: HTMLElement[] | HTMLElement | State;
	root?: boolean;
	event?: EventSchema[] | EventSchema;
	textContent?: string | State;
	attributes?: any | State;
}

export interface InputSchema extends ContainerSchema {
	value?: string;
}

export interface State {
	attribute?;
	element?: HTMLElement;
	prop?: string;
	setState?: (state) => void;
	initial?;
	subscribe?: (callback: (state: any) => void) => void;
}

export const removeAllChildren = (parent: HTMLElement) => {
	Object.values(parent.children).forEach((child: HTMLElement) => child.remove());
};

const addListener = (element, event) => {
	const { name, handler } = event;
	element.addEventListener(name, handler);
};

export const appendChild = (element: HTMLElement, children: HTMLElement[] | HTMLElement) => {
	if (children instanceof Array) {
		children.forEach(child => element.appendChild(child));
	} else {
		if (element instanceof Element) element.appendChild(children);
	}
};

export const update = (element: HTMLElement, config) => {
	const { children = [], attributes = {}, event = [], ...properties } = config;
	if (event) {
		if (event instanceof Array) {
			event.forEach(each => {
				addListener(element, each);
			})
		} else {
			addListener(element, event);
		}
	}

	Object.entries(attributes).forEach(attribute => {
		const [key, value] = attribute;
		element.setAttribute(key, value as string);
	});

	Object.assign(element, properties);

	const styleElement = element as HTMLElement;
	Object.assign(styleElement.style, properties);

	children && appendChild(element, children);
}

export const container = (config: ContainerSchema = {}) => {
	const { tagName = 'div', attributes = {}, root = false, ...updateProps } = config;

	let element: HTMLElement;

	if (root) {
		if (document.querySelector('#app')) {
			element = document.querySelector('#app');
			removeAllChildren(element);
		} else {
			element = document.createElement("div");
			element.id = "app";
			document.body.prepend(element);
		}
	} else {
		element = document.createElement(tagName as string);
	}

	populateContainerState(attributes, element, true)
		.forEach(state => {
			attributes[state.attribute] = state.initial;
		});

	populateContainerState(updateProps, element)
		.forEach(state => {
			updateProps[state.prop] = state.initial;
		});

	update(element, { attributes, ...updateProps });

	return element;
};

export const populateContainerState = (containerProps, element, attribute = false) => {
	return Object.entries(containerProps)
		.filter(([key, value]: [string, State]) => value.setState)
		.map(([key, state]: [string, State]) => {
			state.element = element;
			if (attribute) {
				state.prop = 'attribute';
				state.attribute = key;
			} else {
				state.prop = key;
			}
			return state;
		});
}

export const replaceChild = (parent: HTMLElement, children: HTMLElement) => {
	removeAllChildren(parent);
	appendChild(parent, children);
};

export const animate = (element, newStyle, transition, from) => {
	const { property = 'all', duration = '0s', timing = 'ease', delay = '0s' } = transition;

	from && Object.assign(element.style, from);

	setTimeout(() => {
		Object.assign(element.style, newStyle);
		element.style.transition = `${property} ${duration} ${timing} ${delay}`;
	});
	return element;
};

export const style = (element: HTMLElement, styles: StyleDeclaration) => {
	Object.assign(element.style, styles);
	return element;
};
