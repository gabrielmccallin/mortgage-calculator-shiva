import { removeAllChildren, appendChild, State } from "./container";

export const useState = (initial: State | any, reducer?: (state) => any): [State, any] => {
	let callback;
	if(initial && (<State>initial).setState) {
		(<State>initial).subscribe = (newState => setState(newState));
	}

	const state: State = {
		setState: (state) => {
			setState(state);
		},
		get initial() {
			return reducer && initial ? reducer(initial) : initial;
		},
		set subscribe(subscriberCallback) {
			callback = subscriberCallback;
		}
	};
	const setState = (newState) => {
		const { element, prop, attribute } = state;
		const reducedState = reducer && newState ? reducer(newState) : newState;

		switch (prop) {
			case 'children':
				removeAllChildren(element);
				appendChild(element, reducedState);
				break;

			case 'attribute':
				element.setAttribute(attribute, reducedState);
				break;

			default:
				if (element) {
					element[prop] = reducedState;
					element.style[prop] = reducedState;
				}
				break;
		}
		callback && callback(reducedState);
	};
	return [
		state,
		setState
	]
};