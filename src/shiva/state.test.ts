import { container, useState } from './container';

describe("state", function () {
    it("create container with state children", () => {
        const [children, setChildren] = useState([
            container({textContent: '1'}),
            container({textContent: '2'})
        ]);

        const testContainer = container({
            children
        });

        expect(testContainer.children.length).toEqual(2);
        expect(testContainer.childNodes[0].textContent).toEqual('1');
        expect(testContainer.childNodes[1].textContent).toEqual('2');
        expect(testContainer.hasChildNodes()).toBeTruthy();
    });

    it("update container with state children", () => {
        const [children, setChildren] = useState([
            container({textContent: '1'}),
            container({textContent: '2'})
        ]);

        const testContainer = container({
            children
        });

        setChildren([
            container({textContent: 'NEW'})
        ])

        expect(testContainer.children.length).toEqual(1);
        expect(testContainer.childNodes[0].textContent).toEqual('NEW');
        expect(testContainer.childNodes[1]).toBeUndefined()
        expect(testContainer.hasChildNodes()).toBeTruthy();
    });

    it("update container with state properties", () => {
        const fixture = {
            textContent: 'hello',
            updateTextContent: 'there'
        };

        const [text, setText] = useState(fixture.textContent);

        const testContainer = container({
            textContent: text
        });

        setText(fixture.updateTextContent);
        expect(testContainer.textContent).toEqual(fixture.updateTextContent);
    });

    it("create container with state attribute", () => {

        const [attr, setAttr] = useState('hello');

        const testContainer = container({
            attributes: {
                id: attr
            }
        });

        expect(testContainer.getAttribute('id')).toEqual('hello');
    });

    it("update container with state attribute", () => {

        const [attr, setAttr] = useState('hello');

        const testContainer = container({
            attributes: {
                id: attr
            }
        });

        setAttr('there');
        expect(testContainer.getAttribute('id')).toEqual('there');
    });

    it("update container with className", () => {
        const fixture = {
            initial: 'wow',
            update: 'hey!'
        }
        const [className, setClassName] = useState(fixture.initial);

        const testContainer = container({
            className
        });

        setClassName(fixture.update);
        expect(testContainer.className).toEqual(fixture.update);
    });

    it("update container with state object", () => {
        const fixture = {
            initial: 'wow',
            update: 'hey!'
        }
        const [portal, setPortal] = useState(fixture.initial);

        const [className] = useState(portal);

        const testContainer = container({
            className
        });

        setPortal(fixture.update);
        expect(testContainer.className).toEqual(fixture.update);
    });

    it("create container with state properties", () => {
        const textContentFixture = 'hello';
        const prop: any = {
            get initial() {
                return textContentFixture;
            },
            setState: () => {}
        };

        const testContainer = container({
            textContent: prop
        });

        expect(testContainer.textContent).toEqual(textContentFixture);
    });

    it("useState with null initial", () => {
        const [fixture, setFixture] = useState(null);
        expect(fixture.setState).toBeTruthy();
    });

    it("useState with null initial and reducer", () => {
        const [fixture, setFixture] = useState(null, state => {});
        expect(fixture.setState).toBeTruthy();
    });

    it("useState with null initial and null reducer", () => {
        const [fixture, setFixture] = useState(null, null);
        expect(fixture.setState).toBeTruthy();
    });

    it("useState with null reducer", () => {
        const [fixture, setFixture] = useState(4, null);
        expect(fixture.setState).toBeTruthy();
    });

});
