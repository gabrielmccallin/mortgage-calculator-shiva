import { css } from "emotion";
import { container } from "./shiva/container";
import { styles } from "./styles";

export const line = () => {
    const lineStyle = css({
        height: '1px',
        backgroundColor: styles.lineColour,
        marginBottom: '2rem'
    });

    return container({
        className: lineStyle
    });
};

