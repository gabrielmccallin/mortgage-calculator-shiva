import { css } from 'emotion';
import { calculate } from "./calculate";
import { line } from "./line";
import { parameter } from "./parameter";
import { result } from "./result";
import { useState, container } from "./shiva/container";
import { styles } from "./styles";
import { title } from "./title";

let state = {
    house: 500000,
    savings: 100000,
    mortgage: 440000,
    ltv: 85
};

const onChange = (e) => {
    const { name, value } = e;
    state = { ...state, [name]: value };

    const { left, monthly } = calculate(state);
    setMonthly(monthly);
    setLeft(left);
};

const parameterConfigs = [{
    id: 'calc-house-cost',
    name: 'house',
    value: state.house,
    title: 'Cost of house (£)',
    onChange
}, {
    id: "calc-mortgage",
    title: 'Maximum borrowing (£)',
    name: "mortgage",
    value: state.mortgage,
    onChange
}, {
    id: "calc-savings",
    title: 'Savings (£)',
    name: "savings",
    value: state.savings,
    onChange
}, {
    id: "calc-ltv",
    title: 'Loan to value (%)',
    name: "ltv",
    value: state.ltv,
    onChange
}];

const [left, setLeft] = useState(calculate(state).left, (left: number) => left.toFixed(0));
const [monthly, setMonthly] = useState(calculate(state).monthly, (monthly: number) => monthly.toFixed(2));

const resultConfigs = [{
    title: "Monthly payments (£ / month)",
    id: "result-monthly",
    value: monthly,
    desc: "Repayment"
}, {
    title: "Money left over (£)",
    id: "result-left",
    value: left,
    desc: "Includes stamp duty, legal fees, survey and removal costs"
}];

const centre = css({
    width: '100%',
    maxWidth: '600px',
    margin: 'auto',
    padding: '2rem'
});

const desc = `Enter details to see monthly repayments and money left over. 
Results are approximate.`;

const app = () => {
    container({
        root: true,
        className: css({
            fontFamily: 'Arial, Helvetica, sans-serif',
            color: styles.primaryColour
        }),
        children: [
            container({
                className: centre,
                children: [
                    title({ desc, title: 'Mortgage calculator' }),
                    ...parameterConfigs.map(config => parameter(config)),
                    line(),
                    ...resultConfigs.map(config => result(config))
                ]
            })
        ]
    });
};

app();
